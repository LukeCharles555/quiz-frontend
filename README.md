# QA Synoptic Project

## Apprenticeship Quiz Frontend

## Overview

This project is written for a full stack quiz application. This is where the frontend code is stored which will handle requests to the quiz backend.
The frontend also handles permissions such as being logged in as a guest, a user or an admin.

## Installation

Clone this project to your local machine.

```bash
git clone https://gitlab.com/LukeCharles555/quiz-frontend.git
```

Open the project in an appropriate IDE, and then install all the required dependencies:

```bash
npm install
```

To use this with the full stack functionality, you will have to also set the quiz-backend API. Information on how to do that can be found [here](https://gitlab.com/LukeCharles555/quiz-backend/-/blob/master/README.md)

Once you have set that up and the backend server is up and running, the frontend project is ready to go! Run:

```bash
npm start
```

npm start will start the application on http://localhost:3000/ which you can then access in the browser.

## Configuration

There is only one bit of configuration you need to set up with this project, and that is to create a config.json file within the src/ folder.

An example config file of how to set this up can be found [here](./src/config.json)

## Permissions

There are three levels of permissions in this project.

- Admin
- User
- Guest

Permissions are protected in the front end by custom private routes and conditional rendering. Redux holds the authentication data which is then used throughout the app to figure out if the user is authenticated, and what permissions the user has.

Permissions are also validated in the backend for another layer of security. The user won't be able to intercept the backend to send a request without a valid JWT token that matches up with a user in the database.

Only the admin will have a fully functional experience on this app, as functionality is hidden from lesser permissioned users.

### Admin

The Admin has the ability to create, read (questions and correct/incorrect answers), update and delete questions from the frontend. Of course also the ability to play the quiz. The Admin has to log in through the login page with valid credentials to be granted access.

### User

The User can register for User permissions through the frontend registration page. After they have registered, they will be redirected to the login page, where they can login with their new credentials.
After login, they will be redirected to the quiz page.
The User has the ability to view questions and correct/incorrect answers. They do NOT have the ability to create, update or delete questions.

### Guest

The Guest can bypass the register/login page by going straight to the quiz, either by clicking on a link or going to the http://localhost:3000/quiz URL.
The Guest has no permissions apart from to play the quiz and that's it.
