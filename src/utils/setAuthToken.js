import axios from "axios";

/**
 * @description Function to default the JWT authorization token to every axios request sent
 * @param {JWT token retrieved from the API after valid user login} token
 */
const setAuthToken = (token) => {
  if (token) {
    // Apply authorization token to every request if logged in
    axios.defaults.headers.common["Authorization"] = token;
  } else {
    // Delete auth header
    delete axios.defaults.headers.common["Authorization"];
  }
};
export default setAuthToken;
