/**
 * Added this so that the user never gets the same set of questions
 */
export default function shuffleQuestions(questionsArray) {
  var currentIndex = questionsArray.length,
    temporaryValue,
    randomIndex;

  // While there are elements to shuffle
  while (0 !== currentIndex) {
    // Pick a remaining element
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = questionsArray[currentIndex];
    questionsArray[currentIndex] = questionsArray[randomIndex];
    questionsArray[randomIndex] = temporaryValue;
  }

  return questionsArray;
}
