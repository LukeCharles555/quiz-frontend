/**
 * @description Function that forces the state into specific model for questions
 * @param {State of the form submitted to the function} formState
 * @param {State of the checked radio button for the correct answer} value
 * Note: I would do this schema validation inline with react-hook-form and yup schema validation
 * In the interest of time I have created this as a temporary solution.
 */
const setQuestionModel = (formState, value) => {
  const questionModel = {
    description: formState.description,
    answers: [
      {
        text: formState.answer1,
        isCorrect: value === "1" ? true : false,
      },
      {
        text: formState.answer2,
        isCorrect: value === "2" ? true : false,
      },
      {
        text: formState.answer3,
        isCorrect: value === "3" ? true : false,
      },
      {
        text: formState.answer4,
        isCorrect: value === "4" ? true : false,
      },
    ],
  };

  return questionModel;
};

export default setQuestionModel;
