import setQuestionModel from "./setQuestionModel";

const mockFormState = {
  description: "Test description",
  answer1: "Test1",
  answer2: "Test2",
  answer3: "Test3",
  answer4: "Test4",
};

const mockValue = "1";

const questionModelResponse = {
  description: "Test description",
  answers: [
    {
      text: "Test1",
      isCorrect: true,
    },
    {
      text: "Test2",
      isCorrect: false,
    },
    {
      text: "Test3",
      isCorrect: false,
    },
    {
      text: "Test4",
      isCorrect: false,
    },
  ],
};

describe("setQuestionModel", () => {
  test("should return the question model correctly", () => {
    expect(setQuestionModel(mockFormState, mockValue)).toEqual(
      questionModelResponse
    );
  });
});
