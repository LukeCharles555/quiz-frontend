import React from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { logoutUser } from "../../store/actions/authActions";

const NavigationBar = () => {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  /**
   * To add another level of security handling, we can check if the user is logged in
   * If they are, then we can render a logout button & a link to go and view the questions/answers
   * If they are just a Guest, then only the Home button is rendered
   */

  return (
    <Navbar bg="dark" variant="dark" style={{ height: "80px" }}>
      <Navbar.Brand href="/">WebbiSkools Ltd</Navbar.Brand>
      <Navbar.Collapse className="justify-content-left">
        {auth.isAuthenticated ? (
          <Nav>
            <Navbar.Text>
              Signed in as:{" "}
              <a href="/login">
                {auth.user.name} - {auth.user.roles[0]}
              </a>
            </Navbar.Text>
          </Nav>
        ) : (
          <Navbar.Text>
            Signed in as: <a href="/login">Guest</a>
          </Navbar.Text>
        )}
      </Navbar.Collapse>
      <Navbar.Collapse style={{ marginRight: "5%" }}>
        <Navbar.Text>
          <h3>Quiz Manager</h3>
        </Navbar.Text>
      </Navbar.Collapse>
      {auth.isAuthenticated ? (
        <Nav className="mr-auto">
          {auth.user.roles.includes("Admin") ? (
            <Navbar.Collapse style={{ marginRight: "10px" }}>
              <Nav.Link href="/createQuestion">Create Question</Nav.Link>
            </Navbar.Collapse>
          ) : null}
          <Navbar.Collapse style={{ marginRight: "10px" }}>
            <Nav.Link href="/viewQuestions">View Questions</Nav.Link>
          </Navbar.Collapse>
          <Navbar.Collapse>
            <Button type="button" onClick={dispatch(logoutUser)}>
              Logout
            </Button>
          </Navbar.Collapse>
        </Nav>
      ) : (
        <Nav>
          <Button type="button">
            <Link to="/">Home</Link>
          </Button>
        </Nav>
      )}
    </Navbar>
  );
};

export default NavigationBar;
