import { makeStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

export default makeStyles({
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "20%",
    marginTop: -12,
    marginLeft: -12,
  },
  flex: {
    display: "flex",
  },
});
