import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import axios from "axios";
import { useForm } from "react-hook-form";
import { useSnackbar } from "notistack";
import isEmpty from "lodash/isEmpty";

import useEditQuestionFormStyles from "./EditQuestionForm.styles";

import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  CircularProgress,
} from "@material-ui/core";

import setQuestionModel from "../../../utils/setQuestionModel";

const { baseUrl, questionsUrls } = require("../../../config.json");

const EditQuestionForm = () => {
  const classes = useEditQuestionFormStyles();

  const [question, setQuestion] = useState();
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState("");

  const location = useLocation();
  const { enqueueSnackbar } = useSnackbar();
  const { register, handleSubmit } = useForm();

  useEffect(() => {
    async function getQuestionById() {
      const axiosResult = await axios.get(
        `${baseUrl}${questionsUrls.get}/${location.questionId}`
      );
      const response = await axiosResult.data;
      setQuestion(response);
    }
    getQuestionById();
  }, [location.questionId]);

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleEditQuestion = (newQuestion) => {
    setLoading(true);
    return axios
      .put(`${baseUrl}${questionsUrls.put}/${location.questionId}`, newQuestion)
      .then(() => {
        enqueueSnackbar(
          `Succesfully edited question: ${newQuestion.description}`,
          {
            variant: "success",
          }
        );
      })
      .then(() => {
        setLoading(false);
      })
      .catch((error) => {
        enqueueSnackbar(`Edit question failed: ${error}`, {
          variant: "error",
        });
      });
  };

  const onSubmit = (formState) => {
    handleEditQuestion(setQuestionModel(formState, value));
  };

  return (
    <div>
      {!isEmpty(question) ? (
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl component="fieldset" className={classes.flex}>
            <div
              className="container"
              style={{
                marginTop: "3%",
                marginLeft: "13%",
              }}
            >
              <div className="row">
                <div className="col s8 offset-s2">
                  <Link to="/viewQuestions" className="btn-flat waves-effect">
                    <i className="material-icons left">keyboard_backspace</i>{" "}
                    Back to questions view
                  </Link>
                  <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                    <h4>
                      <b>Edit your question</b> below
                    </h4>
                  </div>
                  <div className="input-field col s12">
                    <input
                      ref={register}
                      required
                      defaultValue={question.description}
                      autoComplete="off"
                      name="description"
                      type="text"
                    />
                    <label htmlFor="description">Description</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      ref={register}
                      required
                      defaultValue={question.answers[0].text}
                      autoComplete="off"
                      name="answer1"
                      type="text"
                    />
                    <label htmlFor="answer1">Answer 1</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      ref={register}
                      required
                      defaultValue={question.answers[1].text}
                      autoComplete="off"
                      name="answer2"
                      type="text"
                    />
                    <label htmlFor="answer2">Answer 2</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      ref={register}
                      required
                      defaultValue={question.answers[2].text}
                      autoComplete="off"
                      name="answer3"
                      type="text"
                    />
                    <label htmlFor="answer3">Answer 3</label>
                  </div>
                  <div className="input-field col s12">
                    <input
                      ref={register}
                      required
                      defaultValue={question.answers[3].text}
                      autoComplete="off"
                      name="answer4"
                      type="text"
                    />
                    <label htmlFor="answer4">Answer 4</label>
                  </div>
                  <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                    <button
                      style={{
                        width: "150px",
                        borderRadius: "3px",
                        letterSpacing: "1.5px",
                        marginTop: "1rem",
                      }}
                      type="submit"
                      disabled={loading}
                      className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                    >
                      Edit Question
                    </button>
                    {loading && (
                      <CircularProgress
                        size={24}
                        className={classes.buttonProgress}
                      />
                    )}
                  </div>
                  <RadioGroup
                    aria-label="isCorrect"
                    style={{
                      marginLeft: "102%",
                      marginTop: "7%",
                    }}
                    name="isCorrect"
                    value={value}
                    onChange={handleChange}
                  >
                    <FormLabel component="legend">Correct Answer</FormLabel>
                    <FormControlLabel
                      style={{ paddingBottom: "34px", paddingTop: "50px" }}
                      value="1"
                      control={<Radio />}
                      label="Answer 1"
                    />
                    <FormControlLabel
                      style={{ paddingBottom: "34px" }}
                      value="2"
                      control={<Radio />}
                      label="Answer 2"
                    />
                    <FormControlLabel
                      style={{ paddingBottom: "34px" }}
                      value="3"
                      control={<Radio />}
                      label="Answer 3"
                    />
                    <FormControlLabel
                      value="4"
                      control={<Radio />}
                      label="Answer 4"
                    />
                  </RadioGroup>
                </div>
              </div>
            </div>
          </FormControl>
        </form>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default EditQuestionForm;
