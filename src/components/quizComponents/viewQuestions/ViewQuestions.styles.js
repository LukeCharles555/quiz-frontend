import { makeStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";

export default makeStyles({
  accordion: {
    width: "80%",
    marginLeft: "8%",
  },
  cardTextDiv: {
    display: "flex",
    justifyContent: "space-between",
  },
  iconDisplay: {
    display: "right",
  },
  cardHeader: {
    display: "flex",
    justifyContent: "space-between",
  },
  checkCircleIcon: {
    color: green[500],
  },
  deleteButton: {
    marginLeft: "20px",
  },
});
