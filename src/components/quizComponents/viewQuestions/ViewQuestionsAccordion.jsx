import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";
import { useSnackbar } from "notistack";

import { Card, Accordion, Button } from "react-bootstrap";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";
import useViewQuestionsStyles from "./ViewQuestions.styles";

const { baseUrl, questionsUrls } = require("../../../config.json");

const ViewQuestionsAccordion = ({ question, id, questionId }) => {
  const classes = useViewQuestionsStyles();
  const auth = useSelector((state) => state.auth);

  const { enqueueSnackbar } = useSnackbar();

  const handleDeleteClick = () => {
    return axios
      .delete(`${baseUrl}${questionsUrls.delete}/${questionId}`)
      .then(() => {
        enqueueSnackbar(`Succesfully deleted question: ${questionId}`, {
          variant: "success",
        });
      })
      .then(() => {
        window.location.reload();
      })
      .catch((error) => {
        enqueueSnackbar(`Delete question failed: ${error}`, {
          variant: "error",
        });
      });
  };

  return (
    <Accordion className={classes.accordion}>
      <Card id={id}>
        <Card.Header className={classes.cardHeader}>
          <Accordion.Toggle as={Button} eventKey="0">
            {question.description}
          </Accordion.Toggle>
          {auth.user.roles.includes("Admin") ? (
            <div>
              <Link
                to={{
                  pathname: "/editQuestion",
                  questionId: questionId,
                }}
              >
                <Button>Edit</Button>
              </Link>
              <Link>
                <Button
                  onClick={handleDeleteClick}
                  variant="danger"
                  className={classes.deleteButton}
                >
                  Delete
                </Button>
              </Link>
            </div>
          ) : null}
        </Card.Header>
        <Accordion.Collapse eventKey="0">
          <div>
            {question.answers.map(
              (answer) =>
                answer._id && (
                  <Card.Body>
                    <div className={classes.cardTextDiv}>
                      <Card.Text>{answer.text}</Card.Text>
                      <div>
                        {answer.isCorrect ? (
                          <CheckCircleIcon
                            className={classes.checkCircleIcon}
                          />
                        ) : (
                          <CancelIcon color="secondary" />
                        )}
                      </div>
                    </div>
                  </Card.Body>
                )
            )}
          </div>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default ViewQuestionsAccordion;
