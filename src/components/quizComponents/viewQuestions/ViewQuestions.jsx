import React, { useState, useEffect } from "react";
import axios from "axios";
import ViewQuestionsAccordion from "./ViewQuestionsAccordion";

const { baseUrl, questionsUrls } = require("../../../config.json");
const ViewQuestions = () => {
  const [questions, setQuestions] = useState([]);

  /**
   * Grab questions when component is loading, if it is loading it will tell the user that
   */
  useEffect(() => {
    async function getQuestions() {
      const axiosResult = await axios.get(`${baseUrl}${questionsUrls.get}`);
      const response = await axiosResult.data;
      setQuestions(response);
    }
    getQuestions();
  }, []);

  const getQuizQuestionElementId = (id) => `question-${id}-card`;

  return (
    <div>
      {questions.length ? (
        <div>
          {questions.map(
            (question) =>
              question._id && (
                <ViewQuestionsAccordion
                  key={question}
                  question={question}
                  id={`${getQuizQuestionElementId(question._id)}`}
                  questionId={question._id}
                />
              )
          )}
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default ViewQuestions;
