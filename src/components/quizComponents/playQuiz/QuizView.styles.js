import { makeStyles } from "@material-ui/core/styles";

export default makeStyles({
  questionHeader: {
    position: "fixed",
    top: "20%",
    left: "40%",
  },
  gridList: {
    width: 500,
    height: 450,
    position: "fixed",
    top: "38%",
    left: "38%",
    marginTop: "-50px",
    marginLeft: "-100px",
  },
  gridListTile: {
    padding: 200,
  },
});
