import React from "react";
import {
  Card,
  Button,
  CardContent,
  CardActions,
  CardHeader,
} from "@material-ui/core";

const QuizAnswerCard = ({ id, answer, isCorrect, selected }) => {
  return (
    <Card style={{ width: "18rem" }} id={id}>
      <CardHeader title="Pick your answer!" />
      <CardContent>{answer}</CardContent>
      <CardActions>
        <Button
          color="primary"
          onClick={() => {
            selected(answer, isCorrect);
          }}
        >
          Select
        </Button>
      </CardActions>
    </Card>
  );
};

export default QuizAnswerCard;
