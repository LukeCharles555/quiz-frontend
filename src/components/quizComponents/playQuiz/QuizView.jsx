import React, { useState, useEffect } from "react";
import axios from "axios";

import QuizAnswerCard from "./QuizAnswerCard";
import { GridList, GridListTile } from "@material-ui/core";
import { useSnackbar } from "notistack";

import shuffleQuestions from "../../../utils/shuffleQuestions";
import useQuizViewStyles from "./QuizView.styles";
import Result from "./Result";

const { baseUrl, questionsUrls } = require("../../../config.json");

const QuizView = () => {
  const [questions, setQuestions] = useState([]);
  const [score, setScore] = useState(0);
  const [responses, setResponses] = useState(0);
  const classes = useQuizViewStyles();
  const { enqueueSnackbar } = useSnackbar();

  async function getQuestions() {
    const axiosResult = await axios.get(`${baseUrl}${questionsUrls.get}`);
    const response = await axiosResult.data;
    const shuffledQuestions = await shuffleQuestions(response);
    setQuestions(shuffledQuestions); // Can shuffle these up so not the same set everytime
  }

  useEffect(() => {
    getQuestions();
  }, []);

  const playAgain = () => {
    getQuestions();
    setScore(0);
    setResponses(0);
  };

  const computeAnswer = (answer, correctAnswer) => {
    if (correctAnswer) {
      setScore(score + 1);
      enqueueSnackbar("CORRECT! Nice one", {
        variant: "success",
      });
    } else {
      enqueueSnackbar("Unlucky...", {
        variant: "error",
      });
    }
    setResponses(responses < 5 ? responses + 1 : 5);
  };

  const getQuizAnswerElementId = (id) => `answer-${id}-card`;

  return (
    <div>
      {questions.length && responses < 5 ? (
        <div>
          <h1>Question {responses + 1}/5</h1>
          <div>
            <h1 className={classes.questionHeader}>
              Question: {questions[responses].description}
            </h1>
            <GridList
              cellHeight={180}
              spacing={20}
              className={classes.gridList}
            >
              {questions[responses].answers.map(
                (answer) =>
                  answer._id && (
                    <GridListTile className={classes.gridListTile} key={answer}>
                      <QuizAnswerCard
                        id={`${getQuizAnswerElementId(answer._id)}`}
                        answer={answer.text}
                        isCorrect={answer.isCorrect}
                        selected={() => computeAnswer(answer, answer.isCorrect)}
                      />
                    </GridListTile>
                  )
              )}
            </GridList>
          </div>
        </div>
      ) : (
        <div>
          {questions.length ? (
            <Result score={score} playAgain={playAgain} />
          ) : (
            <div>Loading...</div>
          )}
        </div>
      )}
    </div>
  );
};

export default QuizView;
