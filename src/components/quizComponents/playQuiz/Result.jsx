import React from "react";
import { Button } from "react-bootstrap";

const Result = ({ score, playAgain }) => {
  const outputMessage = () => {
    let text;

    switch (score) {
      case 0:
        text = "You need to work on your quiz skills!";
        break;
      case 1:
        text = "At least you got 1 answer right...";
        break;
      case 2:
        text = "Could do better!";
        break;
      case 3:
        text = "Over 50%. Proud of you.";
        break;
      case 4:
        text = "Solid work! Try again for 5/5?";
        break;
      case 5:
        text = "And they say Einstein was smart...";
        break;
      default:
        text = `You got ${score}/5 questions right!`;
    }

    return text;
  };
  return (
    <div
      className="score-board"
      style={{ marginLeft: "35%", marginTop: "10%" }}
    >
      <div style={{ marginRight: "48%" }}>
        <div
          className="score"
          style={{ display: "flex", justifyContent: "center" }}
        >
          <h1>You answered {score}/5 questions correctly!</h1>
        </div>
        <div
          className="message"
          style={{ display: "flex", justifyContent: "center" }}
        >
          <h2>{outputMessage()}</h2>
        </div>
      </div>
      <Button
        className="playBtn"
        onClick={playAgain}
        style={{ marginLeft: "18%", marginTop: "5%" }}
      >
        {" "}
        Play Again{" "}
      </Button>
    </div>
  );
};

export default Result;
