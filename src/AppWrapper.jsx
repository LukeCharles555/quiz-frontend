import React from "react";
import { Provider } from "react-redux";
import { SnackbarProvider } from "notistack";
import store from "./store/store";
import App from "./App";

const AppWrapper = () => {
  return (
    <Provider store={store}>
      <SnackbarProvider maxSnack={3}>
        <App />
      </SnackbarProvider>
    </Provider>
  );
};

export default AppWrapper;
