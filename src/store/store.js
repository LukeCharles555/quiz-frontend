import { createStore, applyMiddleware } from "redux";
import thunkMiddleWare from "redux-thunk";
import {
  createStateSyncMiddleware,
  initStateWithPrevTab,
} from "redux-state-sync";
import rootReducer from "./reducers";

const middleware = [thunkMiddleWare, createStateSyncMiddleware({})];
const store = createStore(rootReducer, applyMiddleware(...middleware));
initStateWithPrevTab(store);

export default store;
