import { combineReducers } from "redux";
import { withReduxStateSync } from "redux-state-sync";

import authReducer from "./authReducer";
import errorReducer from "./errorReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  errors: errorReducer,
});

export default withReduxStateSync(rootReducer);
