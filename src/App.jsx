import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import jwt_decode from "jwt-decode";

import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./store/actions/authActions";

import "bootstrap/dist/css/bootstrap.min.css";

import store from "./store/store";

import PrivateRoute from "./components/privateRoute/PrivateRoute";
import AdminPrivateRoute from "./components/privateRoute/AdminPrivateRoute";

import NavigationBar from "./components/layout/NavigationBar";
import Landing from "./components/layout/Landing";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import QuizView from "./components/quizComponents/playQuiz/QuizView";
import ViewQuestions from "./components/quizComponents/viewQuestions/ViewQuestions";
import CreateQuestionForm from "./components/quizComponents/createQuestion/CreateQuestionForm";
import EditQuestionForm from "./components/quizComponents/editQuestion/EditQuestionForm";

const App = () => {
  // Check for token to keep user logged in
  if (localStorage.jwtToken) {
    // Set auth token header auth
    const token = localStorage.jwtToken;
    setAuthToken(token);
    // Decode token and get user info and exp
    const decoded = jwt_decode(token);
    // Set user and isAuthenticated
    store.dispatch(setCurrentUser(decoded));
    // Check for expired token
    const currentTime = Date.now() / 1000; // to get in milliseconds
    if (decoded.exp < currentTime) {
      // Logout user
      store.dispatch(logoutUser());
      // Redirect to login
      window.location.href = "./login";
    }
  }

  console.log(store.getState());

  /**
   * The private routes protect against users with incorrect permissions to go to that URL
   * The components will check first if they are authenticated, if not then it will remove the ability
   * for them to navigate to the page that requires permissions.
   * If they type the 'protected url' into the URL bar, the private route will redirect the user
   * back to the login page
   */
  return (
    <Router>
      <div className="App">
        <NavigationBar />
        <Route exact path="/" component={Landing} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/login" component={Login} />
        <Switch>
          <Route exact path="/quiz" component={QuizView} />
          <PrivateRoute exact path="/viewQuestions" component={ViewQuestions} />
          <AdminPrivateRoute
            exact
            path="/createQuestion"
            component={CreateQuestionForm}
          />
          <AdminPrivateRoute
            exact
            path="/editQuestion"
            component={EditQuestionForm}
          />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
